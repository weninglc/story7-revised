from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib import messages

# Create your views here.


def log_in(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        if User.objects.filter(username=username).exists():
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('/story10/login/')
            else:
                messages.error(request, 'Password doesn\'t match')
        else:
            messages.error(request, 'Username doesn\'t exist')
    return render(request, 'login.html')


def home(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        email = request.POST['email']
        if User.objects.filter(username=username).exists():
            messages.error(request, 'Username is already taken')
        elif User.objects.filter(email=email).exists():
            messages.error(request, 'Email is already taken')
        else:
            user = User.objects.create_user(username, email, password)
            return redirect('/story10/login/')
    return render(request, 'index.html')


def log_out(request):
    logout(request)
    return redirect('/story10/')
