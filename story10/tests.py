from django.test import TestCase, Client, LiveServerTestCase
from django.apps import apps
from django.contrib.auth.models import User
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from .apps import Story10Config
import time

# Create your tests here.


class TestApp(TestCase):
    def testApp(self):
        self.assertEqual(Story10Config.name, 'story10')
        self.assertEqual(apps.get_app_config('story10').name, 'story10')


class UnitTesting(TestCase):
    def test_story10_url_exist(self):
        response = Client().get('/story10/')
        self.assertEqual(response.status_code, 200)

    def test_story10_login_url_exist(self):
        response = Client().get('/story10/login/')
        self.assertEqual(response.status_code, 200)

    def test_story10_logout_url_exist(self):
        response = Client().get('/story10/logout/')
        self.assertEqual(response.status_code, 302)

    def test_create_user(self):
        User.objects.create_user(
            username="wening", email="wening@wening.com", password="wening123")
        self.assertEqual(User.objects.all().count(), 1)

    def test_post_request(self):
        signup_response = Client().post('/story10/',
                                        {'username': 'wening', 'password': '123', 'email': 'wening@gmail.com'})
        self.assertEqual(signup_response.status_code, 302)
        login_response = Client().post('/story10/login/',
                                       {'username': 'wening', 'password': '123'})
        self.assertEqual(signup_response.status_code, 302)


class FunctionalTesting(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.browser = webdriver.Chrome(
            './chromedriver', chrome_options=chrome_options)
        super(FunctionalTesting, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTesting, self).tearDown()

    def test_search_and_like_function(self):
        self.browser.get('%s%s' % (self.live_server_url, '/story10/'))
        self.assertIn('Welcome | Sign Up', self.browser.title)
        time.sleep(3)

        username = self.browser.find_element_by_name('username')
        username.send_keys('anon')
        password = self.browser.find_element_by_name('password')
        password.send_keys('anon123')
        email = self.browser.find_element_by_name('email')
        email.send_keys('anon@anon.com')
        button = self.browser.find_element_by_id('signup')
        button.click()
        time.sleep(3)

        self.assertIn('Welcome', self.browser.title)
        username = self.browser.find_element_by_name('username')
        username.send_keys('anon')
        password = self.browser.find_element_by_name('password')
        password.send_keys('anon123')
        button = self.browser.find_element_by_id('login')
        button.click()
        time.sleep(3)

        welcome_message = self.browser.find_element_by_class_name(
            'main').text
        self.assertIn('anon', welcome_message)

        logout = self.browser.find_element_by_id('logout')
        logout.click()
        time.sleep(3)

        self.assertIn('Welcome | Sign Up', self.browser.title)
