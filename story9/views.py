from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render
from .models import Book
from django.views.decorators.csrf import csrf_exempt
import json

# Create your views here.


@csrf_exempt
def book(request):
    if request.method == 'POST':
        books = list(Book.objects.values()[:5])
        return HttpResponse(json.dumps(books))
    else:
        return render(request, 'book.html')


@csrf_exempt
def like(request):
    if request.method == 'POST':
        id = request.POST['id']
        title = request.POST['title']
        imageLink = request.POST['imageLink']
        author = request.POST['author']
        book, status = Book.objects.get_or_create(
            id=id, title=title, imageLink=imageLink, author=author)
        # print(book)
        book.likes += 1
        book.save()
        return HttpResponse(book.likes)
        # return render(request, 'book.html')
