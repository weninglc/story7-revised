from django.urls import path
from . import views

app_name = 'story9'

urlpatterns = [
    path('book/', views.book, name='book'),
    path('book/like/', views.like, name='like'),
]
