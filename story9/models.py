from django.db import models

# Create your models here.


class Book(models.Model):
    title = models.CharField(max_length=120)
    imageLink = models.CharField(max_length=120)
    author = models.CharField(max_length=120)
    id = models.CharField(max_length=120, primary_key=True, unique=True)
    likes = models.IntegerField(default=0)

    class Meta:
        ordering = ('-likes',)

    def __str__(self):
        return self.title
