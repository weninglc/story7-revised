from django.test import TestCase, Client, LiveServerTestCase
from django.apps import apps
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from .apps import Story9Config
from .models import Book
import time

# Create your tests here.


class TestApp(TestCase):
    def testApp(self):
        self.assertEqual(Story9Config.name, 'story9')
        self.assertEqual(apps.get_app_config('story9').name, 'story9')


class UnitTesting(TestCase):
    def test_story8_url_exist(self):
        response = Client().get('/book/')
        self.assertEqual(response.status_code, 200)

    def test_liked_url_exist(self):
        response = Client().post(
            '/book/like/', {'title': 'buku', 'id': '123', 'imageLink': 'www.www', 'author': 'wening'})
        self.assertEqual(response.status_code, 200)

    def test_model_can_create_new_object(self):
        input = Book(title="buku", likes=0)
        input.save()
        self.assertEqual(Book.objects.all().count(), 1)
        self.assertEqual("buku", str(input))

    def test_post_request_for_top_books(self):
        response = Client().post('/book/')


class FunctionalTesting(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.browser = webdriver.Chrome(
            './chromedriver', chrome_options=chrome_options)
        super(FunctionalTesting, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTesting, self).tearDown()

    def test_search_and_like_function(self):
        # I want to open the website to find the author of my Harry Potter book
        self.browser.get('https://tddnyawening.herokuapp.com/book/')
        self.assertIn('Books', self.browser.title)
        time.sleep(5)

        # Let me type the title first
        search = self.browser.find_element_by_id('search')
        search.send_keys('harry')
        time.sleep(2)
        search.send_keys(' potter')

        # Woah, now I can see the author's name beside the book title!
        time.sleep(5)
        self.assertIn('Harry Potter', self.browser.page_source)
        self.assertIn('Rowling', self.browser.page_source)

        # I want to like the book because I love it a lot, let me click the heart button for 10 times
        heart = self.browser.find_element_by_class_name('like')
        heart.click()
        heart.click()
        heart.click()
        heart.click()
        heart.click()
        heart.click()
        heart.click()
        heart.click()
        heart.click()
        heart.click()
        time.sleep(3)

        # Now let me see if the book is in the top 5 liked books
        topbutton = self.browser.find_element_by_id('top5button')
        topbutton.click()
        time.sleep(2)
        modal = self.browser.find_element_by_class_name('modal-body').text
        self.assertIn('Harry', modal)
