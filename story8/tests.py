from django.test import TestCase, Client, LiveServerTestCase
from django.apps import apps
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from .apps import Story8Config
import time

# Create your tests here.


class TestApp(TestCase):
    def testApp(self):
        self.assertEqual(Story8Config.name, 'story8')
        self.assertEqual(apps.get_app_config('story8').name, 'story8')


class UnitTesting(TestCase):
    def test_story8_url_exist(self):
        response = Client().get('/accordion/')
        self.assertEqual(response.status_code, 200)


class FunctionalTesting(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.browser = webdriver.Chrome(
            './chromedriver', chrome_options=chrome_options)
        super(FunctionalTesting, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTesting, self).tearDown()

    def test_accordion_working(self):
        # Pergi ke halaman /accordion/
        self.browser.get('%s%s' % (self.live_server_url, '/accordion/'))
        self.assertIn('Accordion', self.browser.title)
        time.sleep(5)

        # Tulisan di panel belum terlihat karena dihide sebelum accordion ditekan
        keterangan = self.browser.find_element_by_class_name(
            'accordion-area').text
        self.assertNotIn('Ini accordionnya Wening', keterangan)

        # Ketika accordion ditekan baru muncul keterangan
        accordion = self.browser.find_element_by_id('about')
        accordion.click()
        time.sleep(5)
        keterangan = self.browser.find_element_by_class_name(
            'accordion-area').text
        self.assertIn('Ini accordionnya Wening', keterangan)

        # Waktu ditekan lagi elemennya tulisannya kehide lagi
        accordion.click()
        time.sleep(5)
        keterangan = self.browser.find_element_by_class_name(
            'accordion-area').text
        self.assertNotIn('Ini accordionnya Wening', keterangan)

        # Coba diswap posisi accordion pertama dan kedua
        button = self.browser.find_element_by_xpath(
            "//div[@id='about']//button[@class='down']")
        button.click()
        time.sleep(5)
        judul = self.browser.find_element_by_class_name('accordion-area').text
        self.assertTrue(judul.find('Activities') < judul.find('About'))

        # Coba dikembaliin lagi seperti semula
        button = self.browser.find_element_by_xpath(
            "//div[@id='about']//button[@class='up']")
        button.click()
        time.sleep(5)
        judul = self.browser.find_element_by_class_name('accordion-area').text
        self.assertTrue(judul.find('Activities') > judul.find('About'))

    def test_theme_changer_working(self):
        # Pergi ke halaman /accordion/
        self.browser.get('%s%s' % (self.live_server_url, '/accordion/'))
        self.assertIn('Accordion', self.browser.title)
        time.sleep(5)

        # Simpan variabel yang isinya warna background sebelum diubah
        bg_awal = self.browser.find_element_by_tag_name(
            'body').value_of_css_property('background-color')
        bg_accordion_awal = self.browser.find_element_by_class_name(
            'accordion').value_of_css_property('background-color')

        # Coba tekan salah satu tombol yang ngubah warna
        button = self.browser.find_element_by_class_name('bg-pink')
        button.click()
        time.sleep(3)

        # Bandingin warna awal sama setelah tombol ditekan
        bg_akhir = self.browser.find_element_by_tag_name(
            'body').value_of_css_property('background-color')
        bg_accordion_akhir = self.browser.find_element_by_class_name(
            'accordion').value_of_css_property('background-color')
        self.assertNotEqual(bg_awal, bg_akhir)
        self.assertNotEqual(bg_accordion_awal, bg_accordion_akhir)
