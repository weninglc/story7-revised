from django.test import TestCase, Client, LiveServerTestCase
from django.apps import apps
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from .apps import Story7Config
from .models import Message
from .forms import MessageForm, ConfirmationForm
from .views import homepage, confirmation
import time

# Create your tests here.


class TestApp(TestCase):
    def testApp(self):
        self.assertEqual(Story7Config.name, 'story7')
        self.assertEqual(apps.get_app_config('story7').name, 'story7')

# UNIT TEST


class UnitTesting(TestCase):
    def test_home_url_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_confirmation_url_exist(self):
        response = Client().get('/confirmation/')
        self.assertEqual(response.status_code, 200)

    def test_model_can_create_new_object(self):
        # Test buat objek baru
        input_test = Message(nama="Wening", pesan="Bisa pls")
        input_test.save()
        self.assertTrue(isinstance(input_test, Message))
        self.assertEqual("Message by Wening", str(input_test))

        # Hitung jumlah objek setelah dibuat
        jumlah = Message.objects.all().count()
        self.assertEqual(jumlah, 1)

    def test_form_validation_for_blank_items(self):
        form = MessageForm(data={'nama': '', 'pesan': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['nama'], ["This field is required."])
        self.assertEqual(form.errors['pesan'], ["This field is required."])

    def test_form_validation_accepted(self):
        form = MessageForm(
            data={'nama': 'Weninglagi', 'pesan': 'Yey ngetest form'})
        self.assertTrue(form.is_valid())

    def test_redirect_after_a_POST_request(self):
        response = self.client.post(
            '', data={'nama': 'Wening', 'pesan': 'ini ngetest POST'})
        Client().get('/confirmation/')

    def test_submitted_changed_after_saving_form(self):
        response = Client().get('/?submitted=True')
        self.assertEqual(homepage.submitted, True)

    def test_confirmation_buttons_working(self):
        self.client.post('/confirmation/')

        # Check if form is valid
        form = ConfirmationForm(
            data={'action': 'yes'})
        self.assertTrue(form.is_valid())

        # Test if false
        self.client.post('/confirmation/', data={'action': 'no'})
        # Test if true
        response = self.client.post(
            '', data={'nama': 'Wening', 'pesan': 'ini ngetest POST'})
        self.client.post('/confirmation/', data={'action': 'yes'})


class FunctionalTesting(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.browser = webdriver.Chrome(
            './chromedriver', chrome_options=chrome_options)
        super(FunctionalTesting, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTesting, self).tearDown()

    def test_walkthrough_false(self):
        self.browser.get(self.live_server_url)
        self.assertIn('Spread Your Message!', self.browser.title)
        self.browser.find_element_by_class_name('main')

        # User bakal nemuin header ajakan ngisi form
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('let the world hear\nyour message', header_text)

        # Nyoba input di form
        nama = self.browser.find_element_by_name('nama')
        nama.clear()
        nama.send_keys('ini nyoba')
        pesan = self.browser.find_element_by_name('pesan')
        pesan.clear()
        pesan.send_keys('kalo cancelled')
        submit = self.browser.find_element_by_class_name('submit')
        time.sleep(5)
        submit.click()

        # User bakal keredirect ke web konfirmasi
        self.assertIn('Confirmation', self.browser.title)
        time.sleep(5)

        # Pencet tombol konfirmasi
        rejected = self.browser.find_element_by_class_name('rejected')
        rejected.click()

        # Ada container buat nampilin message-message yang sebelumnya pernah dikirim orang
        self.assertIn('Spread Your Message!', self.browser.title)
        time.sleep(5)
        self.assertNotIn('prev_message', self.browser.page_source)

    def test_walkthrough_true(self):
        # Waktu user buka pagenya, ada tulisan Spread Your Message di title
        self.browser.get(self.live_server_url)
        self.assertIn('Spread Your Message!', self.browser.title)
        self.browser.find_element_by_class_name('main')

        # User bakal nemuin header ajakan ngisi form
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('let the world hear\nyour message', header_text)

        # Nyoba input di form
        nama = self.browser.find_element_by_name('nama')
        nama.clear()
        nama.send_keys('weninghehe')
        pesan = self.browser.find_element_by_name('pesan')
        pesan.clear()
        pesan.send_keys('halohai')
        submit = self.browser.find_element_by_class_name('submit')
        time.sleep(5)
        submit.click()

        # User bakal keredirect ke web konfirmasi
        self.assertIn('Confirmation', self.browser.title)
        time.sleep(5)

        # Pencet tombol konfirmasi
        confirmed = self.browser.find_element_by_class_name('confirmed')
        confirmed.click()

        # Ada container buat nampilin message-message yang sebelumnya pernah dikirim orang
        self.assertIn('Spread Your Message!', self.browser.title)
        preview_box = self.browser.find_element_by_class_name('prev_box')
        preview_name = self.browser.find_element_by_class_name('prev_name')
        preview_message = self.browser.find_element_by_class_name(
            'prev_message')
        time.sleep(5)
