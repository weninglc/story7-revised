from django.db import models

# Create your models here.


class Message(models.Model):
    nama = models.CharField('your name', max_length=120)
    pesan = models.TextField('your message')
    tanggal_dibuat = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "Message by " + self.nama
